﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserMovement : MonoBehaviour
{
    // Lifetime of the laser
    public float lifetime = 2.0f;
    // Speed of the laser
    public float speed = 5.0f;
    // Damage laser will do when it hits .
    public int damage = 1;

    // Use this for initialization
    void Start ()
    {
        // The game object that contains this component will be distroyed
        // after the lifetime ( seconds ) has passed .
        Destroy(gameObject, lifetime);


    }

    // Update is called once per frame
    void Update ()
    {
        transform.Translate(Vector3.up * Time.deltaTime * speed);
    }
}
