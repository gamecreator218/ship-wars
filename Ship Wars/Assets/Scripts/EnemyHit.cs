﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHit : MonoBehaviour
{
    public int health = 2;
    public int pointsPerEnemy = 10;
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnCollisionEnter2D(Collision2D theCollision)
    {
        // Uncomment this line to output the name of colliding object
        // Debug .Log (" Hit " + theCollision . gameObject . name );
        // Try to find a component called laser behaviour in the other object
        LaserMovement laser = theCollision.gameObject.GetComponent<LaserMovement>();
        if (laser != null) // check if it exists .
        {
            // Reduce my health by the damage of the laser .
            health -= laser.damage;
            // Destroy the laser
            Destroy(theCollision.gameObject);
        }
        else
        {
            /* We ’ve hit another ship */
        }
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }
}
