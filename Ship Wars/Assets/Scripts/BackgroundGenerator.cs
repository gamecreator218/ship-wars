﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundGenerator : MonoBehaviour {

    [SerializeField]
    public GameObject tilePrefab;

    // Use this for initialization
    void Start()
    {
        // Check we have a prefab for our tile
        if (tilePrefab.GetComponent<Renderer>() == null)
        {
            // Output an error .
            Debug.LogError(" There is no " +  "renderer available to f-ill background. ");
        }
        else
        {

            // Find out the tile size
            Vector2 tileSize = getTileSize();

            // Find out the size of the screen
           Vector2 screenQuarter = getScreenQuarterSize();

            // Calculate the number of rows and
            // columns which will fit on screen .

            // Ceil is a rounding method ( Ceiling ) - rounds up.
            int columns = Mathf.CeilToInt(screenQuarter.x / tileSize.x);
            int rows = Mathf.CeilToInt(screenQuarter.y / tileSize.y);

            createTiles(rows, columns, tileSize);

            

        }
    }



	
	// Update is called once per frame
	void Update () {
		
	}


    Vector2 getTileSize()
    {
        // Get the sprite renderer attached to the tile
        Renderer rend = tilePrefab.GetComponent<Renderer>();
        // Get the size of its bounds component
        Vector2 size = rend.bounds.size;
        // return this to the calling method .
       return size;
    }
                
    
    // Find out the size of the screen
    Vector2 getScreenQuarterSize()
    {
        // We need to work out how big the screen is from the camera size .
        Camera mainCamera = Camera.main;
        Vector2 quarterOfScreen;
        // orthographic size = 1/2 the vertical viewing volume
        quarterOfScreen.y = mainCamera.orthographicSize;
        // aspect ( aspect ratio ) = width / height
        // so width = aspect * height
        quarterOfScreen.x = mainCamera.aspect * quarterOfScreen.y;
        return quarterOfScreen;
    }

 
    void createTiles(int rows, int columns, Vector2 tileSize)
    {
        // from screen left side to screen right side ,
        // because camera is orthographic .
        for (int c = -columns; c < columns; c++)
        {
            for (int r = -rows; r < rows; r++)
            {
                // Calculate the position of the cetner of each tile .
                // Note : + tileSize .x / 2 - position is the
                // center of the image
                Vector2 position = new Vector2(
                c * tileSize.x + tileSize.x / 2,
                r * tileSize.y + tileSize.y / 2);
                // Create the tiles
                // Position them
                GameObject tile = Instantiate(tilePrefab,
                position,
                Quaternion.identity
                ) as GameObject;
                // Add them to the scene .
                tile.transform.parent = transform;
            }
        }
    }
}
