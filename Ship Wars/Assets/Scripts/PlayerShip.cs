﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerShip : MonoBehaviour
{
    // Hit points
    public int health = 10;

    // Use this for initialization
    void Start ()
    {
		
	}

	
	// Update is called once per frame
	void Update ()
    {
       
    }
    void OnCollisionEnter2D(Collision2D theCollision)
    {
        // Uncomment this line to output the name of
        // colliding object
        // Debug .Log (" Hit " + theCollision . gameObject . name );
        // Try to find a component called MoveTowardsPlayer
        // behaviour in the other object
        MoveTowardsPlayer enemy =
            theCollision.gameObject.GetComponent<MoveTowardsPlayer>();
        if (enemy != null) // check if it exists .
        {
            // Reduce my health by the damage of the laser .
            health -= enemy.collisionDamage;
            Destroy(theCollision.gameObject);
        }
        else
        {
            /* We ’ve hit another player or something else */
        }
        if (health <= 0)
        {
            Destroy(this.gameObject);
        }
    }// end of OnCollisionEnter2D () method .
}// end of the class

