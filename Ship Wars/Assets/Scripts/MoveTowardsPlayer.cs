﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveTowardsPlayer : MonoBehaviour
{

    // private : visible only to this class
    // these are the attributes of the class
    // all methods can see these .
    private Transform player;
    // public : this is visible to other objects
    // Unity makes exposes these in the editor
    // The [ Header ("...")] markup allows us to
    // organise these properties .
    [Header(" Enemy Properties ")]
    public float speed = 0.1f;
    public int collisionDamage = 2;

    // Use this for initialization
    void Start()
    {
        // Has to be this way rather than a
        // serialised field as this is will
        // be a prefab ( when we make waves
        // of enemy ships .
        player = GameObject.Find("PlayerShip").transform;


    }

    // Update is called once per frame
    void Update()
    {
        // Check the player attribute was set in Start ().
        if (player == null)
        {
            Debug.Log(" Cannot find player ship , please " +
            " check its called \" PlayerShip \" ");
        }
        else // This else is repeated from the code above
        {
            // Get the distance the players position to ours .
            Vector3 vectorToPlayer = player.position - transform.position;
            // We want the direction only , not the magnitude
            vectorToPlayer.Normalize();
            // Move the enemy
            Vector3 movement = (vectorToPlayer * speed );
            transform.position = transform.position + movement;

        }
    }
}
